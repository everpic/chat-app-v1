angular.module('eTabatabaAdmin', ['ngRoute'])
.config(($routeProvider, $locationProvider) => {
    $locationProvider.hashPrefix('!');
    $routeProvider.when('/', {
        templateUrl: '/templates/dashboard/index.html',
        controller: 'dashboardController'
    }).when('/stats', {
        templateUrl: '/templates/stats/index.html',
        controller: 'statsController'
    }).when('/users', {
        templateUrl: '/templates/users/index.html',
        controller: 'usersController'
    }).when('/posts', {
        templateUrl: '/templates/posts/index.html',
        controller: 'postsController'
    }).when('/pubs', {
        templateUrl: '/templates/pubs/index.html',
        controller: 'pubsController'
    }).when('/forum', {
        templateUrl: '/templates/forum/index.html',
        controller: 'forumController'
    }).when('/settings', {
        templateUrl: '/templates/settings/index.html',
        controller: 'settingsController'
    }).otherwise('/');
})
.controller('dashboardController', dashboardController)
.controller('statsController', statsController)
.controller('usersController', usersController)
.controller('postsController', postsController)
.controller('pubsController', pubsController)
.controller('settingsController', settingsController)
.controller('forumController', forumController)
.service('custom', function(){
    this.timeFix = (time) => {
        const date = moment(time).format('YYYY-MM-DD HH:mm:ss');
        return moment(date, 'YYYY-MM-DD HH:mm:ss').fromNow();
    }
})