const postsController = function($http, $scope, $interval, custom) {
    $scope.posts = {};

    //defining moment's locale to fr (need it just once in this file)
    moment.locale('fr');

    $scope.fetchPosts = () => {
        $http.get('/admin/posts/getAll').then(res=>{
            res.data.posts.posts.forEach(post=>{
                post.date = custom.timeFix(post.created_at);
            });
            res.data.posts.signaled.forEach(post=>{
                post.date = custom.timeFix(post.created_at);
            });
            $scope.posts = res.data.posts;
        }).catch(err=>console.error(err));
    };

    $scope.fetchPosts();
    let interval = $interval(function(){$scope.fetchPosts()}, 600000);

    //before path change
    $scope.$on('$routeChangeStart', ($event, next, current) => {
        $interval.cancel(interval);
    });
}