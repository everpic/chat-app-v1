const usersController = function($http, $scope, $interval) {
    $scope.users = {};

    $scope.fetchUsers = () => {
        $http.get('/admin/users/getAll').then(res => {
            const users = res.data.users;
            $scope.users = users;
        }).catch(err => {
            console.error(err);
        });
    }
    $scope.fetchUsers();
    let interval = $interval(function(){$scope.fetchUsers()}, 300000);

    //before switching path
    $scope.$on('$routeChangeStart', ($event, next, current)=>{
        $interval.cancel(interval);
    });
}