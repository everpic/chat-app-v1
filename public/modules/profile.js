var ProfileController = function($scope, $http, custom) {
	$scope.posts = [];
	$scope.maskedPosts = [];
	$scope.clientId = user._id;
	$scope.user = user;
	$scope.visited = visited;
	$scope.materials;
    $scope.action = 0;
    $scope.posterText = '';
	$scope.files = 0;
	$scope.materialToDelete = {};
	
	//setting moment's locale to fr
	moment.locale('Fr');
	
	/** edit profile */

	// update profile picture
	$scope.askProfile = () => {
		$('#updateProfileModal').modal('toggle');
	};
	$scope.selectProfile = () => {
		$('#updateProfileModal #file').click();
	};
	$('#updateProfileModal #file').change((e) => {
		const target = $(e.target)[0],
			file = target.files[0],
			type = file.type.split('/', 1);
		if(type == 'image') {
			let reader = new FileReader();
			reader.onload = (e) => {$('#updateProfileModal #profile').attr('src', e.target.result);};
			reader.readAsDataURL(file);
			$('#updateProfileModal #confirmer').show();
			$('#updateProfileModal #selector').hide();
		}
	});
	$scope.saveProfile = () => {
		const form = document.getElementById('form'),
			fd = new FormData(form);
		$http({
			url: '/profile/update/profile',
			method: 'POST',
    		headers: {'Content-Type': undefined},
			data: fd,
			transformRequest: angular.identity
		}).then(res => {
			if(res.data.path) {
				$('.user-avatar').attr('src', res.data.path);
				$scope.cancelProfile();
			}
		}).catch(err => {console.error(err)});
	};
	$scope.cancelProfile =() => {
		$('#updateProfileModal').modal('toggle');
		$('#updateProfileModal #confirmer').hide();
		$('#updateProfileModal #selector').show();
		$('#updateProfileModal #profile').attr('src', $scope.visited.profile);
	};

	//adding new general informations
	$(document).on('click', '.editor', (e) => {
		const element = $(e.target).parent(),
			next = element.next(),
			input = next.children('input');
		element.hide();
		next.show();
		input.focus();
	});

	$(document).on('click', '.canceler', (e) => {
		let element = $(e.target).parent().parent();
		element = (element.hasClass('text-right'))? element.parent() : element;
		const prev = element.prev();
		element.hide();
		prev.show();
	});

	$(document).on('click', '.saver', (e) => {
		let element = $(e.target).parent();
		element = (element.hasClass('btn'))? element.parent() : element;
		const input = element.prev(),
			parent = element.parent(),
			id = input.attr('id'),
			text = input.val(),
			data = {
				field: id,
				content: text
			};
		$http({
			url: '/profile/update/about',
			method: 'POST',
			data: data
		}).then(response => {
			if(response.data.user) {
				let update = response.data.user;
				update[id] = text;
				$scope.visited = update;
				parent.hide();
				const display = parent.prev();
				if(response.data.action == 2) display.show();
			}
		}).catch(err => {
			console.error(err);
		});
	});

	//updating existing general informations
	$scope.startEdit = (e) => {
		$scope.adress = visited.adress;
		$scope.phone = visited.phone;
		$scope.email = visited.email;
		let element = $(e.target).parent().parent();
		element = (element.hasClass('pull-right'))? element.parent() : element;
		const next = element.next(),
			input = next.children('input');
		element.hide();
		next.show();
		input.focus();
	};

	//adding new school
	$scope.startAddSchool = (e) => {
		const element = ($(e.target).hasClass('form-group'))?$(e.target):$(e.target).parent(),
			form = element.prev();
		element.hide();
		form.show();
	};
	$scope.resetSchool = (e)=>{
		const element = ($(e.target).parent().hasClass('pull-right'))?$(e.target).parent():$(e.target).parent().parent(),
			form = element.parent().parent(),
			div = form.next();
		form.hide();
		div.show();
	};
	$scope.saveSchool = (e) => {
		const element = ($(e.target).parent().hasClass('pull-right'))?$(e.target).parent():$(e.target).parent().parent(),
			form = element.parent().parent(),
			div = form.next(),
			schoolYear = form.find('#schoolYear').val(),
			schoolName = form.find('#schoolName').val(),
			data = {
				year: schoolYear,
				name: schoolName
			};
		$http({
			url: '/profile/update/scolar',
			method: 'POST',
			data: data
		}).then(res=>{
			if(res.data.classe) {
				const classe = res.data.classe;
				($scope.visited.scolar)?$scope.visited.scolar.push(classe):$scope.visited.classe = [classe];
				form.hide();
				div.show();
				form.trigger('reset');
			}
		}).catch(err=>{
			console.error(err);
		});
	};

	//adding new material or equipment
	$scope.startAddMaterial = (e) => {
		const element = ($(e.target).hasClass('grey-text'))?$(e.target).parent():$(e.target).parent().parent(),
			form = element.next();
		element.hide();
		form.show();
	};
	$scope.resetMaterial = (form) => {
		const element = $(form.$$element),
			prev = element.prev();
		element.hide();
		prev.show();
	};
	$scope.saveMaterial = (form) => {
		const element = $(form.$$element),
			prev = element.prev();
		$http({
			url: '/profile/update/addMaterial',
			method: 'POST',
			data: {
				type: form.type.$modelValue,
				brand: form.brand.$modelValue,
				model: form.model.$modelValue,
				color: form.color.$modelValue,
				mark: form.mark.$modelValue
			}
		}).then(res=>{
			if(res.data.material) {
				$scope.materials.push(res.data.material);
				element.hide();
				prev.show();
			}
		}).catch(err=>{
			console.error(err);
		});
	};
	//delete an existing material
	$scope.confirmDeleteMaterial = (material) => {
		$scope.materialToDelete = material;
		$('#materialDeletionModal').modal('show');
	};
	$scope.cancelDeleteMaterial = () => {
		$scope.materialToDelete = {};
		$('#materialDeletionModal').modal('hide');
	};
	$scope.deleteMaterial = () => {
		$http.get('/profile/update/deleteMaterial/'+$scope.materialToDelete._id).then(res=>{
			if(res.data.materials) {
				console.log(res.data.materials);
				$scope.user.materials = res.data.materials;
				$scope.materialToDelete = {};
				$('#materialDeletionModal').modal('hide');
			}
		}).catch(err=>{console.error(err)});
	};

	//form button validation
	$scope.schoolValidate = (school) => {
		return (school.schoolName.$invalid || school.schoolYear.$invalid);
	}
	$scope.adressValidate = (form) => {
		return (form.adress.$invalid || form.adress.$modelValue.length < 5);
	}
	$scope.phoneValidate = (form) => {
		return (form.phone.$invalid || form.phone.$modelValue.length < 7);
	}
	$scope.emailValidate = (form) => {
		const pattern = /[a-zA-Z0-9]{3,}@[a-zA-Z0-9]{3,}\.[a-z]{2,}/;
		return (form.email.$invalid || !pattern.test(form.email.$modelValue));
	}
	$scope.materialValidate = (form) => {
		return (form.type.$invalid ||
			(form.brand.$invalid || (form.brand.$modelValue && form.brand.$modelValue.length < 3)) ||
			(form.model.$invalid || (form.model.$modelValue && form.model.$modelValue.length < 3)) ||
			(form.color.$invalid || (form.color.$modelValue && form.color.$modelValue.length < 2))
		);
	}
	/** end edit profile */

	$scope.hasMaterials = () => {
		if($scope.clientId == $scope.visited._id) {
			$scope.materials = $scope.user.materials;
			return ($scope.user.materials.length > 0);
		}
		$scope.materials = $scope.visited.materials;
		return $scope.visited.materials.length > 0;
	}
    
    //use socket.io here

	socket.on('chat message', function(msg) {
		console.log(msg);
	});

    socket.on('newPost', function(data) {
		data.post.date = custom.timeFix(data.post.created_at);
		let posts = Object.keys($scope.posts).map(key => {
			return $scope.posts[key];
		});
		posts.unshift(data.post);
		$scope.posts = custom.arrToObj(posts);
        $scope.$apply();
    });

	socket.on('post loaded', function(data) {
		socket.emit('find post', {id: data.post});
	});

	socket.on('post found', function(data) {
		$scope.posts.unshift(data.post);
		$scope.$apply();
	});
	
	socket.on('liked', function(data) {
		let post = $('.'+data.post+' .postFooter .like span'),
			icone = post.prev(),
			like = parseInt(post.text()),
			index = $scope.posts[data.post];
		if(data.like) {
			post.text(like + 1);
			post.removeClass('ng-hide');
			index.likes.push(data.user);
		}
		else {
			post.text(like -1);
			const userIndex = index.likes.indexOf(data.user);
			index.likes.splice(userIndex, 1);
		}
	});

	socket.on('commented', (data) => {
		if($scope.posts[data.post]) {
			$scope.posts[data.post].comments.push(data.comment);
		}
	});

    socket.on('post deleted', function(data) {
        $('.'+data.post).remove();
        $scope.maskedPosts.push(data.post);
        $scope.$apply();
	});

	//fonction testeuses
	$scope.check = (e) => {
		console.log(e);
	}
};