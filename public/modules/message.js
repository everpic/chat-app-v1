var MessageController = function($scope, $http, custom) {
    $scope.user = client;
    $scope.room = null;
    $scope.users = [];
    $scope.objUsers = {};
    $scope.activeUsers = {};
    $scope.conversations = {};
    $scope.messages = {};
    $scope.groupItems = {};

    $scope.getConversations = () => {
        $http({
            url: '/messages/getConversations',
            method: 'GET'
        }).then(res => {
            $scope.conversations = custom.arrToObj(res.data.conversations);
            // console.log($scope.conversations);
        }).catch(err => {console.error(err)});
    };

    /** new conversation */

    //action: new private conversation
    $scope.createMessage = () => {
        $http.get('/messages/getUsers').then(res => {
            $scope.users = [];
            res.data.users.forEach(user => {if(user._id != $scope.user._id) {$scope.users.push(user)}});
            // console.log($scope.users);
            $('#createMessageModal').modal('toggle');
        }).catch(err => {console.error(err)});
    };
    $scope.privateMessage = (user) => {
        const data = {
            titles: {
                [user._id]: {
                    image: $scope.user.profile,
                    title: $scope.user.username
                },
                [$scope.user._id]: {
                    image: user.profile,
                    title: user.username
                }
            },
            destination: user._id,
            users: [user._id, $scope.user._id]
        };
        $http({
            url: '/messages/privateMessage',
            method: 'POST',
            data: data
        }).then(res => {
            if(res.data.room) {
                $scope.room = res.data.room;
                $('#createMessageModal').modal('hide');
            }
        }).catch(err => {console.error(err)});
    };

    //action: new group message
    $scope.createGroup = () => {
        $http.get('/messages/getUsers').then(res => {
            $scope.users = [];
            res.data.users.forEach(user => {if(user._id != $scope.user._id) {$scope.users.push(user)}});
            $scope.objUsers = custom.arrToObj($scope.users);
            // console.log($scope.users);
            $('#createGroupModal').modal('toggle');
        }).catch(err => {console.error(err)});
    };
    $scope.groupMessage = () => {
        let array = $.map($scope.groupItems, (value, key) => { return [$scope.objUsers[key]]; });
        array.push($scope.user);
        const names = array.map(item => { return item.username; }),
            titles = array.reduce((obj, item) => {
                let list = names.filter(element => {
                    if(element != item.username) {
                        return true;
                    }
                });
                obj[item._id] = `Vous, ${list.join(', ')}`;
                return obj;
            }, {}),
            users = array.map(user => { return user._id });
            // console.log(titles);
        $http({
            url: '/messages/groupMessage',
            method: 'POST',
            data: {
                users: users,
                titles: titles
            }
        }).then(res => {
            if(res.data.room) {
                $scope.room = res.data.room;
                $('#createGroupModal').modal('hide');
            }
        }).catch(err => console.error(err));
    };

    socket.on('room created', (data) => {
        let conversations = custom.objToArr($scope.conversations);
        conversations.unshift(data.room);
        $scope.conversations = custom.arrToObj(conversations);
        $scope.$apply();
    });
    /** ./. new conversation */

    /** select a conversation */
    $scope.selectRoom = (room) => {
        $http.get(`/messages/selectRoom/${room}`).then(res => {
            $scope.messages = custom.arrToObj(res.data.messages);
            // console.log(res.data.messages[0].sender, $scope.user._id);
            $scope.room = res.data.room;
            Object.values($scope.messages).forEach(message => {
                // console.log(message);
                if(message.sender._id != $scope.user._id && message.etat == 0) socket.emit('set seen', {message: message._id});
            });
        }).catch(err => {console.error(err)});
    };
    $scope.contact = (user) => {
        $http.get(`/messages/contact/${user._id}`).then(res => {
            if(res.data.room) $scope.selectRoom(res.data.room._id);
            else {
                $scope.privateMessage(user);
            }
        }).catch(err => console.error(err));
    };
    /** ./. select a conversation */

    /** write and send text messages */
    $scope.messageKeypress = (e) => {
        const event = e.originalEvent,
            target = $(e.target);
        if(!event.ctrlKey && event.key == "Enter") {
            socket.emit('send a message', {room: $scope.room, sender: $scope.user, content: target.val()});
            target.val('');
            e.preventDefault();
        }
        else if(event.ctrlKey && event.key == "Enter") {
            target.val(`${target.val()}\n`);
        }
        // console.log(event);
    };
    /** ./. write and send text messages */

    /** receive and display a message */
    socket.on('new message', (data) => {
        let message = data.message;
        $scope.conversations[message.room].last_message = message;
        if(message.room == $scope.room) {
            $scope.messages[message._id] = message;
            if(message.sender._id != $scope.user._id && message.room == $scope.room) {
                socket.emit('set seen', {message: message._id});
            }
        }
        $scope.$apply();
    });

    socket.on('is seen', (data) => {
        // console.log(data);
        $scope.messages[data.message].etat = 1;
        $scope.conversations[data.room].last_message.etat = 1;
        $scope.$apply();
    });
    /** ./. receive and display a message */

    /** get adn display connected users */
    socket.on('connected', () => {socket.emit('search connected')});
    socket.on('connected list', (data) => {
        $scope.activeUsers = custom.arrToObj(data.users);
        $scope.$apply();
    });
    socket.on('disconnected', () => {socket.emit('search connected')});
    /** ./. get adn display connected users */

    $scope.objectLength = (obj) => { return Object.keys(obj).length; };

    $scope.getConversations();
}