angular.module('chat-app', [])
.controller("MessageController", MessageController)
.controller("PostsController", PostsController)
.controller("ProfileController", ProfileController)
.controller("NotificationController", NotificationController)
.service('custom', function() {
    this.timeFix = (time) => {
        const date = moment(time).format('YYYY-MM-DD HH:mm:ss');
        return moment(date, 'YYYY-MM-DD HH:mm:ss').fromNow();
    };
    this.arrToObj = (array) => {
        const object = array.reduce((object, item) => {
            object[item._id] = item;
            return object;
        },{});
        return object;
    };
    this.objToArr = (obj) => {
        const array = Object.keys(obj).map(key => {
            return obj[key];
        });
        return array;
    }
});