var PostsController = function($http, $scope, custom) {
    $scope.windowHeight = window.innerHeight;
	$scope.action = 0;
	$scope.preview = 0;
	$scope.loading = 0;
	$scope.notify = 0;
    $scope.posterText = '';
    $scope.files = 0;
    $scope.posts = {};
	$scope.clientId = client_id;
	$scope.visited = client_id;
	$scope.postToSignal = {};
	$scope.postToPreview = {};
	$scope.postToShare = {};
	$scope.comment = {};

	//setting moment's locale to fr
	moment.locale('Fr');

    //action : publier quelque chose
    $scope.startPost = function(accept) {
    	$('#innerBlack').fadeIn(400);
        $('#posterInput').removeAttr('accept');
        $('#posterInput').attr('accept', accept);
    	$('#posterInput').click();
    	$scope.action = 1;
		$('.poster').css('z-index', 101);
	};

    $scope.cancelPost = function() {
		$('#innerBlack').fadeOut(400);
		$('.poster').css('z-index', 3);
    	$scope.action = 0;
    	$('#posterText').val('')
    	$scope.files = 0;
    	$('.appended').remove();
    	$('#posterInput').val('');
    };

    $scope.submitPost = function() {
    	$('#posterSubmit').prop('disabled', true);
    	$('#posterSubmit').html('');
    	$('#posterSubmit').html('<i class="fa fa-spinner fa-spin fa-2x"></i>');
    	var url, data;
    	if($scope.files <= 1) {
    		url = "/posts/postOneFile";
    	}
    	else {
    		url = "/posts/postMultipleFiles";
    	}
		var posterForm = document.getElementById('posterForm');
		var fd = new FormData(posterForm);
		fd.append('posterText', $('#posterText').val());
		data = fd;
    	$http({
    		url: url,
    		method: "POST",
    		headers: {'Content-Type': undefined},
    		data: data,
    		transformRequest: angular.identity
    	}).then(function(response) {
			$('#posterSubmit').prop('disabled', false);
			$('#posterSubmit').html('');
			$('#posterSubmit').html('<i class="fa fa-edit"></i> Publier');
			$('#innerBlack').fadeOut(400);
			$('.poster').css('z-index', 3);
			$scope.action = 0;
			$('#posterText').val('');
			if($scope.files > 0) {
				$('.appended').remove();
				$('#posterInput').val('');
			}
			$scope.files = 0;
			socket.emit('newPost', {post: response.data.post});
			flashMessage('Votre publication a été enregistrée');
			// console.log(response);
    	}).catch(function(error) {
    		console.log(error);
    	});
	};

    $('#posterInput').change(function() {
    	$scope.files += this.files.length;
    	$scope.$apply();
    	[...this.files].map(function(element) {
    		var mimeType = element.type.split('/', 1);
    		var content;
    		if(mimeType == "image") {
    			var reader = new FileReader();
    			content = $("<div class='col-sm-3 appended'><img class='img-fluid'></div>");
    			var child = content.children("img");
	    		reader.onload = function(e) {
	    			child.attr('src', e.target.result);
	    		}
	    		reader.readAsDataURL(element);
    		}
    		else if(mimeType == "audio") {
    			content = "<div class='col-sm-3 text-center border border-light appended' style='position: relative; height: 80px; width: 60px;'><i class='fa fa-file-audio-o fa-3x mt-1'></i><div style='position: absolute; bottom: 0; left: 0; width: 100%; height: 30px; font-size: 12px;' class='py-1 grey lighten-3 rounded-top font-weight-bold text-truncate'>"+element.name+"</div></div>";
    		}
    		else if(mimeType == "video") {
    			content = "<div class='col-sm-3 text-center border border-light appended' style='position: relative; height: 80px; width: 60px;'><i class='fa fa-file-video-o fa-3x mt-1'></i><div style='position: absolute; bottom: 0; left: 0; width: 100%; height: 30px; font-size: 12px;' class='py-1 grey lighten-3 rounded-top font-weight-bold text-truncate'>"+element.name+"</div></div>";
    		}
    		else {
    			content = "<div class='col-sm-3 text-center border border-light appended' style='position: relative; height: 80px; width: 60px;'><i class='fa fa-file fa-3x mt-1'></i><div style='position: absolute; bottom: 0; left: 0; width: 100%; height: 30px; font-size: 12px;' class='py-1 grey lighten-3 rounded-top font-weight-bold text-truncate'>"+element.name+"</div></div>";
    		}
    		$('.postPreviews').prepend(content);
    	});
    	var clone = $(this).clone();
	    clone.addClass('appended');
    	if ($(this).hasClass('posterInput')) {
	    	clone.removeAttr('id');
    	}
    	$('#posterForm').append(clone);
    	// $(this).attr('name', 'appendedInput[]');
    	$(this).val('');
    });

    $scope.beginPost = function() {
    	$('#innerBlack').fadeIn(400);
    	$scope.action = 1;
		$('.poster').css('z-index', 101);
    };

    $('#postAppender').click(function() {
    	$('#posterInput').click();
    });

    $scope.postStatus = function() {
    	return ($scope.files == 0 && $('#posterText').val().length <= 3);
    };

    //action : charger fil d'actu
    $scope.loadTimeLine = function() {
    	$http({
    		url: '/posts/loadTimeLine',
    		method: 'GET',
    	}).then(function(response) {
			if(response.data.posts && response.data.posts.length > 0) {
				response.data.posts.forEach(post=>{
					post.date = custom.timeFix(post.created_at);
					if(post.type == 2) {
						$http.get(`/posts/getOriginalPost/${post.original}`).then(res => {
							let original = res.data.original;
							original.date = custom.timeFix(original.created_at);
							post.original = original;
						}).catch(err => {console.error(err)});
					}
				});
				$scope.posts = custom.arrToObj(response.data.posts);
                setTimeout(function() {
                    $('.carousel').carousel({
                        interval: 2000,
                        pause: 'hover'
                    });
				}, 2000);
			}
    	}).catch(function(error) {
    		console.log(error);
    	});
    };

    //action : like/unlike a post
    $scope.like = function(postId) {
    	$http({
    		url: '/posts/like/'+postId,
    		method: "GET",
    	}).then(function(response) {
    	}).catch(function(error) {
    		console.log(error);
    	});
	};
	
	//action comment a post
	$scope.commentText = (e) => {
		const target = $(e.target),
			text = target.val();
		if(text.length > 0) {
			$scope.comment.text = text;
		}
	};

	$scope.submitComment = (id) => {
		$http({
			url: `/posts/comment/${id}`,
			method: 'POST',
			data: $scope.comment
		}).then(res=> {
			if(res.data.ok) {
				$scope.comment.text = '';
			}
		}).catch(err=>console.error(err));
	};

	//action : share a post
	$scope.askShare = (post) => {
		$scope.postToShare = post;
		$('#shareModal').modal('toggle');
		$('#shareOption').material_select();
	};

	$scope.share = () => {
		const data = {};
		data.post = $scope.postToShare._id;
		data.label = $('#shareText').val();
		data.option = $('#shareOption').children('option:selected').val();
		$http({
			url: '/posts/share',
			method: 'POST',
			data: data
		}).then(res => {
			let post = res.data.post;
			$http.get(`/posts/getOriginalPost/${post.original}`).then(res => {
				let original = res.data.original;
				original.date = custom.timeFix(original.created_at);
				post.original = original;
				socket.emit('newPost', {post: post});
				$('#shareModal').modal('toggle');
				flashMessage('Actualité patagée');
			}).catch(err => {console.error(err)});
		}).catch(err => { console.error(err) });
	};

	$('#shareOption').change(function() {
		const value = $(this).children('option:selected').val();
		if(parseInt(value) == 2) $('#shareDestinationWrapper').show();
		else $('#shareDestinationWrapper').hide();
	});

	$('#shareDestination').keypress((e) => {
		let value = $(e.target).val();
		if(value.length > 1) {
			$http({
				url: '/posts/searchDestination',
				method: 'GET'
			}).then(res => {
				let array = res.data.content,
					copy = [];
				if(res.data.type == 1) {
					array.forEach(item => {
						copy.push(item.title);
					});
				}
				else if(res.data.type == 2) {
					array.forEach(item => {
						if(item._id != $scope.clientId) {
							copy.push(item.username);
						}
					});
				}
				$('#shareDestination').mdb_autocomplete({
					data: copy
				});
			}).catch(err => {console.error(err)});
		}
	});

	$('#shareModal').on('hide.bs.modal', (e) => {
		$('#shareText').val('');
		$('#shareText').next().removeClass('active');
		$('#shareDestination').val('');
		$('#shareDestination').next().next().removeClass('active');
	});

    //action: delete a post
    $scope.confirmDelete = function(id) {
        $('#postToDelete').val(id);
        $('#deletionModal').modal('show');
    }

    $scope.deletePost = function() {
        var id = $('#postToDelete').val();
        $http({
            url: '/posts/delete/'+id,
            method: "GET"
        }).then(function(response) {
            if(response.data.post) {
                $scope.cancelDeletion();
            }
            else {
                flashMessage('Une erreur est survenue.');
            }
        }).catch(function(error) {
            console.log(error);
        });
    }

    $scope.cancelDeletion = function() {
        $('#postToDelete').val('');
        $('#deletionModal').modal('hide');
	}
	
	// action: signal a post
	$scope.confirmSignal = (id) => {
		$scope.postToSignal = {};
		$('#signalModal').modal('show');
		$('.mdb-select').material_select();
		$scope.postToSignal.id = id;
		$('#signalModal').on('hide.bs.modal', (e)=>{
			$('.mdb-select').material_select('destroy');
		});
	}
	$scope.signalPost = () => {
		if($scope.postToSignal.purpose) {
			$scope.postToSignal.reporter = $scope.clientId;
			$http.post('/posts/signal', $scope.postToSignal).then(res=>{
				if(res.data.reported) {
					$('#signalModal').modal('hide');
					$scope.postToSignal = {};
					flashMessage('Publication signalée.');
				}
			}).catch(err=>console.error(err));
		}
		// $('#signalModal').modal('hide');
	}
	$scope.cancelSignal = () => {
		$scope.postToSignal = {};
		$('#signalModal').modal('hide');
	}

	/** post preview */
	$scope.postPreview = (post) => {
		let images = post.attachments.filter(file=> { if(file.srcType && file.srcType.split('/')[0] == "image") return true });
		$scope.postToPreview = post;
		if(images.length > 0) {
			//display the lightbox
			const lightbox = $('#lightbox'),
				preview = $('#lightbox #preview'),
				img = preview.children('img');
			lightbox.show();
			img.attr('src', images[0].src);
			lightbox.addClass('scale');
			preview.show();
			$scope.preview = 1;

			//close the lightbox
			$(document).keyup((e) => {
				if((e.key === "Escape" || e.key === "Esc" || e.keyCode == 27 || e.which == 27) && $scope.preview == 1) {
					closeLightbox();
				}
			});
		}
	}
	$('#close').click(() => { closeLightbox() });
	const closeLightbox = () => {
		$scope.preview = 0;
		const lightbox = $('#lightbox'),
			preview = $('#lightbox #preview');
		lightbox.fadeOut(300);
		lightbox.removeClass('scale');
		preview.hide();
		$scope.postToPreview = {};
	}
	/** end post preview */

	/** about notifications */
	$scope.displayNotifications = (e) => {
		const notifications = $('#notificationContent').clone();
		notifications.removeClass('d-none');
		const target = ($(e.target).hasClass('mdi') || $(e.target).hasClass('red'))?$(e.target).parent():$(e.target);
		target.popover({
			content: notifications,
			placement: 'bottom',
			trigger: 'click',
			html: true
		});
		console.log(notifications);
	};

    //use socket.io here

	socket.on('chat message', function(msg) {
		console.log(msg);
	});

    socket.on('newPost', function(data) {
		data.post.date = custom.timeFix(data.post.created_at);
		let posts = Object.keys($scope.posts).map(key => {
			return $scope.posts[key];
		});
		posts.unshift(data.post);
		$scope.posts = custom.arrToObj(posts);
        $scope.$apply();
    });

	socket.on('post loaded', function(data) {
		socket.emit('find post', {id: data.post});
	});

	socket.on('post found', function(data) {
		$scope.posts.unshift(data.post);
		$scope.$apply();
	});

	socket.on('liked', function(data) {
		let post = $('.'+data.post+' .postFooter .like span'),
			icone = post.prev(),
			like = parseInt(post.text()),
			index = $scope.posts[data.post];
		if(data.like) {
			post.text(like + 1);
			post.removeClass('ng-hide');
			index.likes.push(data.user);
		}
		else {
			post.text(like -1);
			const userIndex = index.likes.indexOf(data.user);
			index.likes.splice(userIndex, 1);
		}
	});

	socket.on('commented', (data) => {
		if($scope.posts[data.post]) {
			$scope.posts[data.post].comments.push(data.comment);
			try {
				$scope.$apply();
			} catch (error) {
				console.log(error);
			}
		}
	});

    socket.on('post deleted', function(data) {
		delete $scope.posts[data.post];
        $scope.$apply();
	});

	socket.on('set notification', (data) => {
		socket.emit('send notification', data);
	});

	socket.on('shared', (data) => {
		if($scope.posts[data.post]) {
			if($scope.posts[data.post].shares) $scope.posts[data.post].shares.push(data.id);
			else $scope.posts[data.post].shares = [data.id];
		}
	});
	
	$scope.loadTimeLine();

	//start carousels if any
	setTimeout(function() {
		$('.carousel').carousel({
			interval: 2000,
			pause: 'hover'
		});
	}, 2000);

	//utilities functions
    var flashMessage = function(text) {
        $('#flashMessage').text(text);
        $('#flashMessage').fadeIn(300);
        setTimeout(function(){
			$('#flashMessage').fadeOut(200);
			$('#flashMessage').text('');
		},2000);
	}

    // $(document).ready(function() {
    //     socket.emit('checker', {post: '5b69f1f29f254c1da0569487'});
    // });
}