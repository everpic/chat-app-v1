const NotificationController = function($scope, $http, custom) {
    $scope.getNotifications = () => {
        $http({
            url: '/notification/getNotifications',
            method: 'GET'
        }).then(res => {
            console.log(res.data);
        }).catch(err => {console.error(err)});
    }

    $scope.getNotifications();

    socket.on('notify', (data) => {
        console.log(data);
    });

    const key = 'BOMRF0ImbCOFMOC-A6XfBvbLNIo8yhu0KKPLOpIpAtt6JG0vLhY_REUooeGdybuYAfUuViUfK_HPnkw5Hdoo-SI';
    if('serviceWorker' in navigator) {
        // send().then(res=>console.log(res)).catch(err => console.error(err));
    }

    // async function send() {
    //     navigator.serviceWorker.register('/public/js/worker.js');
    //     navigator.serviceWorker.ready.then(function(registration) {
    //         registration.pushManager.subscribe({
    //             userVisibleOnly: true,
    //             applicationServerKey: urlBase64ToUint8Array(key)
    //         }).then(function(subscription) {
    //             return subscription;
    //         });
    //     });
    // }

    function urlBase64ToUint8Array (base64String) {
        const padding = '='.repeat((4 - base64String.length % 4) % 4);
        const base64 = (base64String + padding).replace(/\-/g, '+').replace(/_/g, '/');
        const rawData = window.atob(base64);
        const outputArray = new Uint8Array(rawData.length);

        for(let i = 0; i < rawData.length; ++i) {
            outputArray[i] = rawData.charCodeAt(i);
        }
        return outputArray;
    }
};