const User = require('../models/User'),
    Session = require('../models/Session'),
    Post = require('../models/Post'),
    Attachment = require('../models/Attachment'),
    Comment = require('../models/Comment'),
    Material = require('../models/Material');
const multer = require('multer');

exports.index = function(req, res) {
    User.findById(req.params.userId).populate('materials').exec(function(err, user) {
        if(err) return console.log(err);
        else {
            if(user) {
                Session.findOne({'user_id':req.session.user_id}).populate({path: 'user_id', populate: {path: 'materials'}}).exec((err, client) => {
                    if(err) return console.error(err);
                    else if(client) res.render('profile/index', {title: user.username, client: client.user_id, visited: user});
                    else res.redirect('/');
                    // res.send(req.session.user_id);
                });
            }
            else {
                res.redirect('/');
            }
        }
    });
}

exports.updateAbout = (req, res) => {
    const body = req.body;
    User.findByIdAndUpdate(req.session.user_id,{[body.field]: body.content}).exec((err, user) => {
        if(err) return console.error(err);
        else {
            let action = (user[body.field])? 2 : 1;
            res.send({user: user, action: action});
        }
    });
}

exports.updateScolar = (req, res) => {
    const body = req.body;
    User.findByIdAndUpdate(req.session.user_id, {$push: {'scolar': body}}).exec((err, user)=>{
        if(err) return console.log(err);
        else {
            res.send({classe: body});
        }
    });
}

exports.addMaterial = (req, res) => {
    const body = req.body;
    User.findById(req.session.user_id).exec((err,user)=>{
        if(err) return console.error(err);
        else {
            let material = new Material;
            material.type = body.type;
            material.brand = body.brand;
            material.model = body.model;
            material.color = body.color;
            material.owner = user._id;
            if(body.mark)material.mark = body.mark;
            material.save((err, newMaterial)=>{
                if(err) return console.error(err);
                else {
                    user.update({$push: {'materials': newMaterial._id}}).exec((err, user, numAffected)=>{
                        if(err) return console.error(err);
                        else {
                            res.send({material: newMaterial});
                        }
                    });
                }
            });
        }
    });
}

exports.deleteMaterial = (req, res) => {
    Material.findByIdAndRemove(req.params.materialId).exec((err, material)=>{
        if(err) return console.error(err);
        else {
            User.findByIdAndUpdate(req.session.user_id).exec((err, user)=>{
                if(err) return console.error(err);
                else {
                    let materials = [];
                    if(user.materials.length > 1) {
                        const index = user.materials.findIndex(material._id);
                        materials = user.materials.splice(index, 1);
                    }
                    res.send({materials: materials});
                }
            });
        }
    });
}

exports.updateProfile = (req, res) => {
    const diskStorage = multer.diskStorage({
        destination: (req, file, cb) => {
            cb(null, './uploads/avatars');
        },
        filename: (req ,file, cb) => {
			var extension = file.originalname.split('.');
			cb(null, `${Date.now()}_${file.fieldname}.${extension[extension.length - 1]}`);
        }
    });
    const upload = multer({storage: diskStorage}).single('avatar');
    upload(req, res, (err) => {
        if(err) return console.error(err);
        else {
            const file = req.file;
            User.findByIdAndUpdate(req.session.user_id, {'profile': `/${file.path}`}).exec((err, user) => {
                if(err) return console.error(err);
                else {
                    res.send({path: `/${file.path}`});
                }
            });
        }
    });
}