var User = require('../models/User'),
	SocketModel = require('../models/Socket'),
	Session = require('../models/Session'),
	Message = require('../models/Message'),
	Room = require('../models/Room');

const config = require('../config'),
	io = config.io,
	siofu = config.siofu;

exports.index = function(req, res) {
	Session.findOne({'user_id': req.session.user_id}).populate('user_id').exec((err, session) => {
		if(err) return console.error(err);
		else if(session) {
			if(session.user_id.activeConnections.length > 0) {
				req.session.rooms = session.user_id.activeConnections;
			}
			res.render('messages/index', {client: session.user_id, title: 'E-boss | Messages'});
		}
		else res.redirect('/');
	});
};

exports.getConversations = (req, res) => {
	User.findById(req.session.user_id).populate({path: 'activeConnections', populate: {path: 'last_message', populate: {path: 'sender', select: 'profile'}}}).exec((err, user) => {
		if(err) return console.error(err);
		else {
			res.send({conversations: user.activeConnections});
		}
	});
};

exports.getUsers = (req, res) => {
	User.find().exec((err, users) => {
		if(err) return console.error(err);
		else {
			res.send({users: users});
		}
	});
};

exports.privateMessage = (req, res) => {
	const body = req.body;
	let room = new Room;
	room.connected_users = body.users;
	room.titles = body.titles;
	room.save((err, created) => {
		if(err) return console.error(err);
		else {
			res.send({room: created._id});
			created.connected_users.forEach(user => {
				SocketModel.findOne({'user': user}).exec((err, item) => {
					if(err) return console.error(err);
					else if(item && io.sockets.connected[item.socket_id]) {
						io.sockets.connected[item.socket_id].join(created._id);
						created.last_message = {
							content: 'Nouvelle conversation.',
							etat: 1
						};
						io.to(item.socket_id).emit('room created', {room: created});
					}
				});
				User.findByIdAndUpdate(user, {$push: {'activeConnections': created._id}}).exec((err, user) => {if(err) return console.error(err);});
			});
		}
	});
};

exports.groupMessage = (req, res) => {
	const body = req.body;
	let newRoom = new Room;
	newRoom.connected_users = body.users;
	newRoom.type = 2;
	newRoom.titles = body.titles;
	newRoom.save((err, room) => {
		if(err) console.error(err);
		else {
			res.send({room: room._id});
			room.connected_users.forEach(user => {
				SocketModel.findOne({'user': user}).exec((err, item) => {
					if(err) return console.error(err);
					else if(item && io.sockets.connected[item.socket_id]) {
						io.sockets.connected[item.socket_id].join(room._id);
						room.last_message = {
							content: 'Nouvelle conversation.',
							etat: 1
						};
						io.to(item.socket_id).emit('room created', {room: room});
					}
				});
				User.findByIdAndUpdate(user, {$push: {'activeConnections': room._id}}).exec((err, user) => {if(err) return console.error(err);});
			})
		}
	});
};

exports.contact = (req, res) => {
	const users = [req.session.user_id, req.params.userId];
	Room.findOne({$or: [{'connected_users': users}, {'connected_users': users.reverse()}]}).exec((err, room) => {
		if(err) return console.error(err);
		else res.send({room: room});
	});
};

exports.selectRoom = (req, res) => {
	Message.find({'room': req.params.roomId}).populate({path: 'sender', select: 'profile'}).sort('created_at').exec((err, messages) => {
		if(err) return console.error(err);
		else {
			res.send({room: req.params.roomId, messages: messages});
		}
	});
};

exports.deleteRooms = (req, res) => {
	Room.find().exec((err, rooms) => {
		if(rooms.length > 0) {
			rooms.forEach(room => {
				Room.findByIdAndRemove(room._id).exec((err, removed) => {
					if(err) return console.error(err);
					else {
						removed.connected_users.forEach(user => {
							User.findByIdAndUpdate(user, {$set: {'activeConnections': []}}).exec((err, deleted) => {if(err) return console.error(err);});
						});
					}
				});
			});
			res.send(rooms);
		}
	});
};

/**
* using socket.io here
*/

io.on('connection', function(socket) {
	Session.findOneAndUpdate(
    	{'session_id': socket.handshake.session.id},
    	{'socket_id': socket.id},
    	function(err, result) {
    		if(err) return console.log(err);
    		else {
    			if(result != null) {
    				SocketModel.findOne({'user': socket.handshake.session.user_id}).exec(function(err, utilisateur){
    					if(err) return console.log(err);
    					else if(utilisateur == null) {
    						var socket_new = new SocketModel;
    						socket_new.socket_id = socket.id;
    						socket_new.user = socket.handshake.session.user_id;
    						socket_new.save(function(err, result) {
    							if(err) return console.log(err);
    							else {
    								search_connected(socket.handshake.session.user_id);
    								join_rooms(socket.handshake.session.rooms);
    							}
    						});
    					}
    					else {
    						utilisateur.update({'socket_id': socket.id}).exec(function(err, result) {
	    						search_connected(socket.handshake.session.user_id);
	    						join_rooms(socket.handshake.session.rooms);
    						});
    					}
						socket.handshake.session.socket_id = socket.id;
						socket.broadcast.emit('connected');
    				});
    			}
	    			
    		}
		});

	socket.on('disconnect', () => {
		SocketModel.findOneAndRemove({'socket_id': socket.id}, (err, removed) => {
			if(err) return console.error(err);
			else if(removed) {
				socket.emit('disconnected');
			}
		});
	});

	socket.on('search connected', () => {
		search_connected(socket.handshake.session.user_id);
	});
		
	socket.on('send a message', (data) => {
		let message = new Message;
		message.room = data.room;
		message.sender = data.sender._id;
		message.type = 1;
		message.content = data.content;
		message.save((err, saved) => {
			if(err) return console.error(err);
			else {
				Room.findByIdAndUpdate(saved.room, {'last_message': saved._id}).exec((err, room) => {
					if(err) return console.error(err);
					else {
						const send = {
							content: saved.content,
							_id: saved._id,
							sender: {_id: saved.sender, profile: data.sender.profile},
							etat: 0,
							room: saved.room
						};
						io.to(saved.room).emit('new message', {message: send});
						// socket.emit('message sent', {message: send});
					}
				});
			}
		});
	});

	socket.on('set seen', (data) => {
		Message.findByIdAndUpdate(data.message, {'etat': 1}).exec((err, message) => {
			if(err) return console.error(err);
			else io.to(message.room).emit('is seen', {message: message._id, room: message.room});
		});
	});

	var search_connected = (user) => {
		SocketModel.find({'user': {$nin: [user]}}).populate({path: 'user', select: 'profile username'}).exec(function(err, list) {
			if(err) return console.log(err);
			else {
				socket.emit('connected list', {users: list});
			}
		});
	};
});


var join_rooms = function (table) {
	if(table) {
		table.forEach(function(element) {
			Room.findById(element).exec(function(err, room) {
				if(room != null){
					room.connected_users.forEach(function(user) {
						SocketModel.findOne({'user': user}).exec(function(err, item) {
							if(item != null && item != undefined){
								if(io.sockets.connected[item.socket_id]) {
									io.sockets.connected[item.socket_id].join(room._id);
								}
								else {
									SocketModel.findByIdAndRemove(item._id).exec();
								}
							}
						});
					});
				}
			});
		});
	}
};

module.exports.io = io;
