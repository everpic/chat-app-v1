const Session = require('../models/Session'),
    Post = require('../models/Post'),
    User = require('../models/User');

exports.index = (req ,res) => {
    Session.findOne({'session_id': req.session.id}).populate('user_id').exec((err, user) => {
        if(err) return console.error(err);
        else if(user){
            res.render('tests/index', {client: user.user_id});
        }
        else res.redirect('/');
    });
}

exports.fixUserNotif = (req, res) => {
    User.findByIdAndUpdate(req.params.userId, {$set: {'notifications': []}}).exec((err, user) => {
        if(err) return console.error(err);
        else {
            res.send(user);
        }
    });
}

exports.fixPost = (req, res) => {
    Post.findByIdAndUpdate(req.params.postId, {$set: {'likes': []}}).exec((err, post) => {
        if(err) return console.error(err);
        else res.send(post);
    });
}

const isInArray = function(array, item) {
    return array.some(function(like) {
        return like.equals(item);
    });
}
var tester = function(likes, sender) {
    if(isInArray(likes, sender)) {
        return true;
    }
    else {
        return false;
    }
};