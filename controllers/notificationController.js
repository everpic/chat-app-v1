const NotificationModel = require('../models/Notification'),
    User = require('../models/User'),
    Post = require('../models/Post'),
    SocketModel = require('../models/Socket')
    webPush = require('web-push');

const config = require('../config'),
    io = config.io;

const vapidKeys = {
    private: '-8NIlqPEsGob6bMEL4yw65mn5Ytib_wWfo2j8QmB2co',
    public: 'BOMRF0ImbCOFMOC-A6XfBvbLNIo8yhu0KKPLOpIpAtt6JG0vLhY_REUooeGdybuYAfUuViUfK_HPnkw5Hdoo-SI'
};

webPush.setVapidDetails('mailto:tokyratefinanahary97@gmail.com', vapidKeys.public, vapidKeys.private);

exports.getNotifications = (req, res) => {
    User.findById(req.session.user_id, 'notifications').populate('notifications').exec((err, notifications) => {
        if(err) return console.error(err);
        else {
            res.send({notifications: notifications});
        }
    });
}
exports.push = (req, res) => {
    const payload = JSON.stringify({title: 'Push test'});
    res.status(201);
    webPush.sendNotification(req.body, payload).catch(err => console.error(err));
}

io.on('connection', (socket) => {
    socket.on('send notification', (data) => {
        Post.findById(data.post).populate('sender').populate('likes').exec((err, post) => {
            if(err) return console.error(err);
            else {
                let likes = post.likes, string;
                if(isInArray(post.likes, post.sender)) {
                    post.depopulate('likes');
                    likes.splice(post.likes.indexOf(post.sender._id), 1);
                }
                if(likes.length == 2) {
                    if(likes[0]._id == data.notifier._id) string = `${data.notifier.username} et ${likes[1].username} aiment votre publication.`;
                    else string = `${data.notifier.username} et ${likes[0].username} aiment votre publication.`;
                }
                else if(likes.length > 2) {
                    string = `${data.notifier.username} et ${likes.length - 1} aiment votre publication.`;
                }
                else {
                    string = `${data.notifier.username} aime votre publication.`;
                }
                NotificationModel.findByIdAndUpdate(data.id, {'label': string}).exec((err, notification) => {
                    if(err) return console.error(err);
                    else {
                        SocketModel.findOne({'user': post.sender._id}).exec((err, found) => {
                            if(err) return console.error(err);
                            else if(found) {
                                notification.label = string;
                                socket.to(found.socket_id).emit('notify', {notification: notification});
                            }
                        });
                    }
                });
            }
        });
    });

    socket.on('subscribed', (data) => {
        const payload = JSON.stringify({title: 'Push test'});

        console.log(data.registration);

        setTimeout(function(){webPush.sendNotification(data.registration, payload).catch(err => console.error(err))}, 3000);
    });
});

const isInArray = (array, item) => {
    return array.some(element=> {
        return item.equals(element);
    });
}