const User = require('../models/User'),
    Session = require('../models/Session'),
    SocketModel = require('../models/Socket'),
    Post = require('../models/Post');

const multer = require('multer');

exports.index = (req, res) => {
    Session.findOne({'session_id': req.session.id}).populate('user_id').exec((err, session) => {
        if(err) return console.error(err);
        else if(session) {
            const user = session.user_id;
            if(user.level && user.level >= 3)
                res.render('admin/index', {title: `Admin | ${user.username}`, client: user, token: req.session.rememberToken});
            else res.redirect('/posts');
        }
        else res.redirect('/');
    });
}

exports.register = (req, res) => {
    // const diskStorage = multer.diskStorage({
    //     destination: (req, file, cb) => {
    //         cb(null, './uploads/avatars')
    //     },
    //     filename: (req, file, cb) => {
    //         const extension = file.originalname.split('.');
    //         cb(null, Date.now()+'_'+file.fieldname+'.'+extension[extension.length - 1]);
    //     }
    // });
    // const upload = multer({storage: diskStorage}).single('avatar');
    // upload(req, res, (err) => {
    //     if(err) return console.error(err);
    //     else {
    //         const user = new User,
    //             body = req.body;
    //         user.nom = body.nom;
    //         user.prenom = body.prenom;
    //         user.username = body.nom+' '+body.prenom;
    //         user.type
    //     }
    // });
    const user = new User,
        { nom, prenom, password, identity } = req.body;
    user.nom = body.nom;
    user.prenom = body.prenom;
    user.username = prenom+' '+nom;
    user.password = password;
    user.matricule = identity;
    user.save((err, user) => {
        if(err) return console.error(err);
        else {
            res.send(user);
        }
    });
}

//users methods
exports.getUsers = (req, res) => {
    User.find().exec((err, users) => {
        if(err) return console.error(err);
        else {
            SocketModel.find().populate('user').exec((err, sockets)=>{
                if(err) return console.error(err);
                else {
                    res.send({
                        users: {
                            students: users.filter(user => user.level == 1),
                            teachers: users.filter(user=>user.level == 3),
                            users: users,
                            online: {
                                students: sockets.filter(socket=>socket.user.level == 1),
                                teachers: sockets.filter(socket=>socket.user.level == 3)
                            }
                        }
                    });
                }
            });
        }
    });
}

//posts methods
exports.getPosts = (req, res) => {
    Post.find().populate('sender').populate('attachments').exec((err, posts) => {
        if(err) return console.error(err);
        else {
            res.send({
                posts: {
                    posts: posts,
                    signaled: posts.filter(post=>post.signaled.length > 0)
                }
            });
        }
    });
}