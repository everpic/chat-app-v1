const User = require('../models/User');

exports.auth = (req, res) => {
    User.findById(req.session.user_id, '-password').exec((err, user) => {
        if(err) return console.error(err);
        else {
            res.send(user);
        }
    });
}