var Session = require('../models/Session'),
	User = require('../models/User'),
	multer = require('multer'),
	hash = require('password-hash-and-salt'),
	moment = require('moment');

exports.auth = function(req, res) {
	Session.findOne({'session_id': req.session.id}).exec(function(err, result) {
		if(err) return console.log(err);
		else {
			if(result != null) {
				res.redirect('posts');
			}
			else {
				res.render('index', {title: 'E-tabataba | Connexion ou Inscription'});
			}
		}
	});
	// console.log(Session);
}

exports.login = function(req, res) {
	var sess = req.session,
		form = req.body;
	User.findOne({'matricule': form.username}).exec(function(err, user) {
		if(err) return console.log(err)
		else if(user == null){
			res.render('index', {title: 'E-tabataba | Connexion ou Inscription', error: 'Nom d\'utilisateur incorrect.'});
		}
		else {
			hash(form.password).verifyAgainst(user.password, function(error, verified) {
		        if(error)
		            throw new Error('Something went wrong!');
		        if(!verified) {
		        	res.render('index', {title: 'E-tabataba | Connexion ou Inscription', error: 'Mot de passe incorrect.', user: user});
		        } else {
					var session = new Session;
					session.session_id = req.session.id;
					session.username = user.username;
					session.user_id = user._id;
					hash(moment().format('YYYY/MM/DD')+user._id).hash((err, token)=> {
						if(err) return console.error(err);
						else {
							session.rememberToken = token;
							session.save(function(err, session) {
								if(err) return console.log(err);
								else{
									req.session.user_id = user._id;
									req.session.rememberToken = session.rememberToken;
									res.redirect('posts');
								}
							});
						}
					});
		        }
		    });
		}
	});
}

exports.subscribe = function(req, res) {
	Session.findOne({'session_id': req.session.id}).exec(function(err, result) {
		if(err) return console.log(err);
		else {
			if (result == null) {
				res.render('inscription', {title: 'E-tabataba | Inscription'});
			}
			else {
				res.redirect('messages');
			}
		}
	});
}

exports.register = function(req, res) {
	var diskStorage = multer.diskStorage({
		destination: function(req, file, cb) {
			cb(null, './uploads/avatars');
		},
		filename: function(req, file, cb) {
			var extension = file.originalname.split('.');
			cb(null, Date.now()+'_'+file.fieldname+'.'+extension[extension.length - 1]);
		}
	});
	var upload = multer({storage: diskStorage}).single('avatar');
	upload(req, res, function(err) {
		if(err) {
			console.log(err);
			return
		}
		else {
			var form = req.body;
			var file = req.file;
			var user = new User;
			user.nom = form.nom;
			user.prenom = form.prenom;
			user.matricule = form.matricule;
			user.classe = form.matricule.split('/')[0];
			hash(form.password).hash(function(error, password) {
				if(error) return console.log(error);
				else {
					user.password = password;
					user.username = form.prenom+' '+form.nom;
					if(file != undefined) {
						var path = `/${file.path.replace(/\\/g, "/")}`;
						user.profile = path;
					}
					user.save(function(err, user, numAffected) {
						if(err) return console.log(err);
						else {
							var sess = new Session;
							sess.session_id = req.session.id;
							sess.user_id = user._id;
							hash(moment().format('YYYY/MM/DD')+user._id).hash((err, token)=> {
								if(err) return console.error(err);
								else {
									sess.rememberToken = token;
									sess.save(function(err, session, numAffected) {
										if(err) return console.log(err)
										else{
											req.session.user_id = user._id;
											req.session.rememberToken = session.rememberToken;
											res.redirect('posts');
										}
									});
								}
							});
						}
					});
				}
			});
		}
	});
}

exports.logout = function(req, res) {
	Session.findOne({'session_id': req.session.id}).exec(function(err, result) {
		if(err)
			return console.log(err)
		else if(result == null)
			res.redirect('/');
		else {
			var sess = req.session;
			req.session.destroy(function(err) {
				if(err) return console.log(err)
				else {
					Session.deleteOne({'session_id': sess.id}).then(function(next) {
						res.redirect('/');
					});
				}
			});
		}
	});
}

exports.verify = (req, res) => {
	Session.findOneAndRemove({'rememberToken': req.params.token}).populate('user_id').exec((err, session)=>{
		if(err) return console.error(err);
		else if(session){
			const old = session;
			session.depopulate('user_id');
			let sess = new Session;
			sess.session_id = req.session.id;
			sess.user_id = session.user_id;
			hash(moment().format('DD/MM/YYYY')+session.user_id).hash((err, token)=>{
				if(err) return console.error(err);
				else {
					sess.rememberToken = token;
					sess.save((err, newSession)=>{
						if(err) return console.error(err);
						else {
							req.session.user_id = newSession.user_id;
							req.session.rememberToken = newSession.rememberToken;
							(old.user_id.level == 4)?res.redirect('/admin'):res.redirect('/posts');
						}
					});
				}
			});
		}
		else {
			res.redirect('/');
		}
	});
}