const multer = require('multer');

const Session = require('../models/Session'),
	User = require('../models/User'),
	SocketModel = require('../models/Socket'),
	Room = require('../models/Room'),
	Post = require('../models/Post'),
	Attachment = require('../models/Attachment'),
	Comment = require('../models/Comment'),
	Notify = require('../models/Notification');

const conf = require('../controllers/messages_controller'),
	io = conf.io;

const fs = require('fs');

exports.index = function(req, res) {
	Session.findOne({'session_id': req.session.id}).exec(function(err, result) {
		if(err) return console.log(err);
		else {
			if(result) {
				User.findById(result.user_id).exec(function(err, user) {
					if(user.activeConnections.length > 0) {
						req.session.rooms = user.activeConnections;
					}
					res.render('posts/index', {title: 'E-tabataba', client: user, token: req.session.rememberToken});
				});
			}
			else {
				res.redirect('/');
			}
		}
	});
};

exports.timeLine = function(req, res) {
	Post.find().populate('attachments')
				.populate('sender')
				.populate({path: 'comments', populate: {path: 'sender'}})
				.sort('-created_at')
				.limit(15)
				.exec(function(err, posts) {
					if(err) return console.log(err);
					else {
						res.send({posts: posts});
					}
				});
};

exports.postOneFile = function(req, res) {
	var diskStorage = multer.diskStorage({
		destination: function(req, file, cb) {
			var fileType = file.mimetype.split('/', 1);
			if(fileType == 'image') {
				cb(null, './uploads/files/images');
			}
			else if(fileType == 'audio') {
				cb(null, './uploads/files/audio');
			}
			else if(fileType == 'video') {
				cb(null, './uploads/files/videos');
			}
			else {
				cb(null, './uploads/files/files');
			}
		},
		filename: function(req, file, cb) {
			var extension = file.originalname.split(".");
			extension = extension[extension.length - 1];
			cb(null, `${Date.now()}_${file.fieldname}.${extension}`);
		}
	});
	var upload = multer({storage: diskStorage}).single('posterInput');
	upload(req, res, function(err) {
		if(err) return console.log(err);
		else {
			// res.send(req.file);
			var file = req.file;
			var post = new Post;
			post.scope = 1;
			if(req.body.posterText) {
				post.content = req.body.posterText
			}
			post.sender = req.session.user_id;
			if(file) {
				var fileType = file.mimetype.split("/", 1);
				var attachment = new Attachment;
				if(fileType == "image") {
					attachment.type = 1;
				}
				else if(fileType == "audio") {
					attachment.type = 2;
				}
				else if(fileType == "video") {
					attachment.type = 3;
				}
				else {
					attachment.type = 4;
				}
				attachment.src = `/${file.path.replace(/\\/g, "/")}`;
				attachment.srcType = file.mimetype;
				attachment.originalName = file.originalname;
				attachment.save(function(err, saved) {
					if(err) return console.log(err);
					else {
						post.attachments = [saved._id];
						post.save(function(err, result) {
							if(err) return console.log(err);
							else {
								Post.findById(result._id)
									.populate('attachments')
									.populate('sender')
									.exec(function(err, post) {
										if(err) return console.log(err);
										else {
											res.send({post: post});
										}
									});
							}
						});
					}
				});
			}
			else {
				post.save(function(err, result) {
					if(err) return console.log(err);
					else {
						post.populate('sender', function(err, post) {
							res.send({post: post});
						});
					}
				});
			}
		}
	});
};

exports.postMultipleFiles = function(req, res) {
	var diskStorage = multer.diskStorage({
		destination: function(req, file, cb) {
			var fileType = file.mimetype.split('/', 1);
			if(fileType == 'image') {
				cb(null, './uploads/files/images');
			}
			else if(fileType == 'audio') {
				cb(null, './uploads/files/audio');
			}
			else if(fileType == 'video') {
				cb(null, './uploads/files/videos');
			}
			else {
				cb(null, './uploads/files/files');
			}
		},
		filename: function(req, file, cb) {
			var extension = file.originalname.split(".");
			extension = extension[extension.length - 1];
			cb(null, `${Date.now()}_${file.fieldname}.${extension}`);
		}
	});
	var upload = multer({storage: diskStorage}).any();
	upload(req, res, function(err) {
		if(err) return console.log(err);
		else {
			var post = new Post;
			post.scope = 1;
			if(req.body.posterText) {
				post.content = req.body.posterText;
			}
			post.sender = req.session.user_id;
			post.save(function(err, newPost) {
				if(err) return console.log(err);
				else {
					if(newPost) {
						var length = req.files.length;
						var increment = 1;
						var id = newPost._id;
						res.send({message: 'wait', post: newPost});
						req.files.forEach(function(file) {
							var fileType = file.mimetype.split("/", 1);
							var attachment = new Attachment;
							if(fileType == "image") {
								attachment.type = 1;
							}
							else if(fileType == "audio") {
								attachment.type = 2;
							}
							else if(fileType == "video") {
								attachment.type = 3;
							}
							else {
								attachment.type = 4;
							}
							attachment.src = `/${file.path.replace(/\\/g, "/")}`;
							attachment.srcType = file.mimetype;
							attachment.originalName = file.originalname;
							attachment.save(function(err, newFile) {
								if(err) return console.log(err);
								else {
									Post.findByIdAndUpdate(id, {$push: {'attachments': newFile._id}}).exec(function(err, saved) {
										if(err) return console.log(err);
										else {
											if(increment == length) {
												SocketModel.findOne({"user": req.session.user_id}).exec(function(err, socket){
													io.sockets.connected[socket.socket_id].emit('post loaded', {post: saved._id});
												});
											}
											else {
												increment++;
											}
										}
									});
								}
							});
						});
					}
				}
			});
		}
	});
};

exports.like = function(req, res) {
	Post.findById(req.params.postId).populate('sender').exec(function(err, post) {
		if(err) return console.log(err);
		else {
			var isInArray = function(array, item) {
				return array.some(function(like) {
					return like.equals(item);
				});
			}
			if(isInArray(post.likes, req.session.user_id)) {
				post.update({$pull: {'likes': req.session.user_id}}, function(err, postUp) {
					if(err) return console.log(err);
					else {
						res.send({post: post._id});
						io.sockets.emit('liked', {post: post._id, user: req.session.user_id});
					}
				});
			}
			else {
				post.update({$push: {'likes': req.session.user_id}}).exec(function(err, postUp) {
					if(err) return console.log(err);
					else {
						if(req.session.user_id != post.sender._id) {
							let notification = new Notify;
							notification.origin = `/posts/${post._id}`;
							notification.image = post.sender.profile;
							notification.type = 2;
							notification.action = 1;
							notification.save((err, notif) => {
								if(err) return console.error(err);
								else {
									SocketModel.findOne({'user': req.session.user_id}).populate('user').exec((err, socket) => {
										if(err) return console.error(err);
										else {
											io.sockets.connected[socket.socket_id].emit('set notification', {id: notif._id, post: post._id, action: 1, notifier: socket.user});
											post.update({$set: {'likeNotification': notif._id}}).exec((err,postUp)=>{if(err) return console.error(err)});
											User.findByIdAndUpdate(post.sender._id, {$push: {'notifications': notif._id}}).exec((err, user)=>{if(err) return console.error(err)});
										}
									});
								}
							});
						}
						res.send({post: post._id, like: true});
						io.sockets.emit('liked', {post: post._id, user: req.session.user_id, like: true});
					}
				});
			}
		}
	});
};

exports.comment = (req, res) => {
	Post.findById(req.params.postId).exec((err, post) => {
		if(err) return console.error(err);
		else {
			let comment = new Comment;
			comment.type = 1;
			comment.sender = req.session.user_id;
			comment.content = req.body.text;
			comment.save((err, comment) => {
				if(err) return console.error(err);
				else {
					post.update({$push: {'comments': comment._id}}).exec((err, saved) => {
						if(err) return console.error(err);
						else {
							Comment.findById(comment._id).populate('sender').exec((err, commentaire) => {
								if(err) return console.error(err);
								else {
									res.send({ok: true});
									io.sockets.emit('commented', {comment: commentaire, post: post._id});
								}
							});
						}
					});
				}
			});
		}
	});
};

exports.searchDestination = (req, res) => {
	User.findById(req.session.user_id).populate('activeConnections').exec((err, rooms) => {
		if(err) return console.error(err);
		else if(rooms.length > 0) {
			res.send({type: 1, content: rooms});
		}
		else {
			User.find().sort('username').exec((err, users) => {
				if(err) return console.error(err);
				else {
					res.send({type: 2, content: users});
				}
			});
		}
	});
};

exports.share = (req ,res) => {
	Post.findByIdAndUpdate(req.body.post, {$push: {'shares': req.session.user_id}}).exec((err, post) => {
		if(err) return console.error(err);
		else {
			let share = new Post;
			share.sender = req.session.user_id;
			share.content = req.body.label;
			share.original = req.body.post;
			share.type = 2;
			share.save((err, shared) => {
				if(err) return console.error(err);
				else {
					shared.populate('sender', (err, post) => {
						res.send({post: post});
					});
				}
			});
			io.sockets.emit('shared', {post: post._id, id: req.session.user_id});
		}
	});
};

exports.giveOriginalPost = (req, res) => {
	Post.findById(req.params.postId).populate('sender').populate('attachments').populate('comments').exec((err, post) => {
		if(err) return console.error(err);
		else res.send({original: post});
	});
};

exports.delete = function(req, res) {
	Post.findByIdAndRemove(req.params.postId).exec(function(err, post) {
		if(err) console.log(err);
		else {
			res.send({post: post});
			io.sockets.emit('post deleted', {post: post._id});
			if(post.comments) {
				post.comments.forEach(function(comment) {
					Comment.findByIdAndRemove(comment._id).exec(function(err, comment) {
						if(err) console.log(err);
					});
				});
			}
			if(post.attachments.length > 0) {
				post.attachments.forEach(file=>{
					Attachment.findByIdAndRemove(file._id).exec((err, attachment)=>{
						if(err) return console.error(err);
						 else {
							 fs.unlink(`.${attachment.src}`, (err) => {
								 if(err) return console.error(err);
							 });
						 }
					});
				});
			}
		}
	});
};

exports.signal = (req, res) => {
	Post.findById(req.body.id).exec((err, post) => {
		if(err) return console.error(err);
		else {
			const body = req.body;
			let report = {};
			report.reporter = body.reporter;
			report.purpose = body.purpose;
			post.update({$push: {'signaled': report}}, (err, updated) => {
				if(err) return console.error(err);
				else {
					if(updated.ok) {
						res.send({reported: true});
					}
				}
			});
		}
	});
};

exports.authorize = (req, res) => {
	Post.findByIdAndUpdate(req.params.postId, {$set: {'signaled': []}}).exec((err, post, next) => {
		if(err) return console.error(err);
		else {
			res.send({post: post, next: next});
		}
	});
};

/**
* using socket.io here
*/
// io.use(sharedSession(sessionInit, {autoSave: true}));
io.on('connection', function(socket) {	
	socket.on('find post', function(data) {
		Post.findById(data.id)
			.populate('attachments')
			.populate('sender')
			.exec(function(err, post) {
				if(err) return console.log(err);
				else {
					socket.emit('post found', {post: post});
				}
			});
	});

	socket.on('checker', function(data) {
		socket.emit('increment comment', {post: data.post});
	});

	socket.on('newPost', function(data) {
		io.sockets.emit('newPost', {post: data.post});
	});

	socket.on('set notification', (data) => {
		console.log(data);
	});
});

/** utilities functions */
const isInArray = function(array, item) {
    return array.some(function(like) {
        return like.equals(item);
    });
}