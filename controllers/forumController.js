const Session = require('../models/Session');

exports.index = (req, res) => {
    Session.findOne({'user_id': req.session.user_id}).populate('user_id').exec((err, session) => {
        if(err) return console.error(err);
        else if(session){
            res.render('forum/index', {client: session.user_id, title: 'E-boss | Forum'});
        }
        else res.redirect('/');
    });
}

exports.goToAsk = (req, res) => {
    Session.findOne({'user_id': req.session.user_id}).populate('user_id').exec((err, session) => {
        if(err) return console.error(err);
        else if(session){
            res.render('forum/ask', {client: session.user_id, title: 'E-boss | Forum'});
        }
        else res.redirect('/');
    });
}

exports.questions = (req, res) => {
    console.log(req.session);
}

exports.question = (req, res) => {
    console.log(req.params);
}