const config = require('./config'),
	createError = require('http-errors'),
	path = require('path'),
	cookieParser = require('cookie-parser'),
	logger = require('morgan'),
	favicon = require('serve-favicon');

const indexRouter = require('./routes/index'),
	messageRouter = require('./routes/message_router'),
	postRouter = require('./routes/posts_router'),
	profileRouter = require('./routes/profile_router'),
	adminRouter = require('./routes/adminRouter'),
	testRouter = require('./routes/testRouter'),
	notificationRouter = require('./routes/notificationsRouter'),
	forumRouter = require('./routes/forumRouter');

const app = config.app,
	server = config.server,
	express = config.express,
	sessionInit = config.sessionInit;

// about database
var mongoose = require('mongoose');
mongoose.connect('mongodb://Buu97:LS4wt7e8@ds235850.mlab.com:35850/buu97');
// mongoose.connect('mongodb://127.0.0.1:27017/devbase');

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
	console.log('database connected');
});


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(sessionInit);
app.use(cookieParser());

//static files
app.use('/public', express.static(path.join(__dirname, 'public')));
app.use('/uploads', express.static(path.join(__dirname, 'uploads')));
app.use('/templates', express.static(path.join(__dirname, '/views/admin/templates')));

app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));

// using routes
app.use('/', indexRouter);
app.use('/messages', messageRouter);
app.use('/posts', postRouter);
app.use('/profile', profileRouter);
app.use('/admin', adminRouter);
app.use('/test', testRouter);
app.use('/notification', notificationRouter);
app.use('/forum', forumRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports.app = app;
module.exports.server = server;
