const mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    GroupSchema = new Schema({
        created_at: {type: Date, default: Date.now},
        name: String,
        description: String,
        members: [{type: Schema.Types.ObjectId, ref: 'User'}]
    });

module.exports = mongoose.model('Group', GroupSchema);