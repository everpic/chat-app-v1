var mongoose = require('mongoose');
	var Schema = mongoose.Schema;
	var SessionSchema = new Schema({
		created_at: {type: Date, default: Date.now},
		session_id: String,
		user_id: {type: Schema.Types.ObjectId, ref: 'User'},
		socket_id: {type: String, default: null},
		rememberToken: String
	});
module.exports = mongoose.model('Session', SessionSchema);