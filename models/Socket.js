var mongoose = require('mongoose'),
	Schema = mongoose.Schema,
	User = mongoose.model('User'),
	socketSchema = new Schema({
		created_at: {type: Date, default: Date.now},
		socket_id: String,
		user: {type: Schema.Types.ObjectId, ref: 'User'}
	}, {collection: 'sockets'});

module.exports = mongoose.model('Socket', socketSchema);