var mongoose = require('mongoose'),
	Schema = mongoose.Schema,
	commentSchema = new Schema({
		created_at: {type: Date, default: Date.now},
		sender: {type: Schema.Types.ObjectId, ref: 'User'},
		type: Number,
		src: {type: String, default: null},
		content: {type: String, default: null}
	}, {collection: 'comments'});
module.exports = mongoose.model('Comment', commentSchema);