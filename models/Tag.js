const mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    tagSchema = new Schema({
        created_at: {type: Date, default: Date.now},
        libelle: String,
        description: {type: String, default: null}
    });

module.exports = mongoose.model('Tag', tagSchema);