const mongoose = require('mongoose'),
    moment = require('moment'),
    Schema = mongoose.Schema,
    MockSchema = new Schema({
        created_at: {type: Date, default: moment(new Date)}
    });

module.exports = mongoose.model('Mock', MockSchema);