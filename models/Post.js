var mongoose = require('mongoose'),
	Schema = mongoose.Schema;
	postSchema = new Schema({
		created_at: {type: Date, default: Date.now},
		scope: Number,
		likes: [{type: Schema.Types.ObjectId, ref: 'User'}],
		likeNotification: {type: Schema.Types.ObjectId, ref: 'Notification', default: null},
		comments: [{type: Schema.Types.ObjectId, ref: 'Comment'}],
		commentNotification: {type: Schema.Types.ObjectId, ref: 'Notification', default: null},
		shares: [{type: Schema.Types.ObjectId, ref: 'User'}],
		content: {type: String, default: null},
		sender: {type: Schema.Types.ObjectId, ref: 'User'},
		attachments: [{type: Schema.Types.ObjectId, ref: 'Attachment'}],
		signaled: [{reporter: {type: Schema.Types.ObjectId, ref: 'User'}, purpose: Number}],
		authorized: {type: Boolean, default: false},
		tags: [{type: Schema.Types.ObjectId, ref: 'Tag', default: null}],
		original: {type: Schema.Types.ObjectId, ref: 'Post', default: null},
		type: {type: Number, default: 1}
	}, {collection: 'posts'});
module.exports = mongoose.model('Post', postSchema);