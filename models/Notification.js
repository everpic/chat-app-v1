const mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    notification = new Schema({
        created_at: {type: Date, default: Date.now},
        origin: String, //the link where it points to
        label: String, //the title of the notificationn
        seen: {type: Boolean, default: false},
        about: {type: String, default: null}, //the text of the post/question the notification is about
        image: {type: String, default: null}, // will be the icon off the web push. the value is the path to the emitter's profile but only if triggered by like or comment.
        type: Number, // 1 = warning, 2 = posts, 3 = forum, 4 = groups, 5 = info
        action: {type: Number, default: null} // only for posts. 1= like, 2= comment
    }, {'collection': 'notifications'});

module.exports = mongoose.model('Notification', notification);