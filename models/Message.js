const mongoose = require('mongoose'),
	Schema = mongoose.Schema,
	messageSchema = new Schema({
		created_at: {type: Date, default: Date.now},
		room: {type: Schema.Types.ObjectId, ref: 'Room'},
		sender: {type: Schema.Types.ObjectId, ref: 'User'},
		src: {type: String, default: null}, // only if type == 2
		srcType: {type: String, default: null}, // only if type == 2
		fileName: {type: String, default: null}, // only if type == 2
		type: Number, // 1 = text, 2 = file
		content: String,
		etat: {type: Number, default: 0} // 0 = not seen, 1 = seen
	}, {'collection' : 'messages'});

module.exports = mongoose.model('Message', messageSchema);