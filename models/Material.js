const mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    MaterialSchema = new Schema({
        created_at: {type: Date, default: Date.now},
        type: Number,
        owner: {type: Schema.Types.ObjectId, ref: 'User'},
        brand: {type: String, default: null},
        model: {type: String, default: null},
        color: {type: String, default: null},
        mark: {type: String, default: null}
    });
    MaterialSchema.virtual('icon').get(()=>{
        if(this.type == 1) return 'mdi mdi-laptop';
        else if (this.type == 2) return 'mdi mdi-camera-iris';
        else return 'fa fa-gavel';
    });

module.exports = mongoose.model('Material', MaterialSchema);