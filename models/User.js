var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var usersSchema = new Schema({
	created_at: {type: Date, default: Date.now},
	nom: String,
	prenom: String,
	username: {type: String, default: null},
	password: String,
	// cursus: {type: String, required: [true, 'Le cursus doit être défini.']},
	// filiere: {type: String, required: [true, 'La filière doit être définie.']},
	matricule: {type: String, unique: true},
	profile: {type: String, default: '/public/images/avatars/male.jpg'},
	phone: {type: String, default: null},
	email: {type: String, default: null},
	adress: {type: String, default: null},
	quartier: {type: String, default: null},
	tags: [{type: String, default: null}],
	materials: [{type: Schema.Types.ObjectId, ref: 'Material', default: null}],
	groups: [{type: Schema.Types.ObjectId, ref: 'Group', default: null}],
	classe: String,
	scolar: [],
	activeConnections: [{type: Schema.Types.ObjectId, ref: 'Room', default: null}],
	level: {type: Number, default: 1},
	addedBy: {type: Schema.Types.ObjectId, ref: 'User', default: null},
	notifications: [{type: Schema.Types.ObjectId, ref: 'Notification', default: null}]
}, {'collection' : 'users'});

module.exports = mongoose.model('User', usersSchema);