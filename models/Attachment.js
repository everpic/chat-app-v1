var mongoose = require('mongoose'),
	Schema = mongoose.Schema,
	attachmentSchema = new Schema({
		created_at: {type: Date, default: Date.now},
		type: Number,
		src: String,
		srcType: String,
		originalName: String
	});
module.exports = mongoose.model('Attachment', attachmentSchema);