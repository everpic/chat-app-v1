var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var roomSchema = new Schema({
	created_at: {type: Date, default: Date.now},
	connected_users: [{type: Schema.Types.ObjectId, ref: 'User'}],
	image: {type: String, default: '/public/images/avatars/male.jpg'},
	last_message: {type: Schema.Types.ObjectId, ref: 'Message', default: null},
	type: {type: Number, default: 1},
	titles: {}
}, {'collection': 'rooms'});

module.exports = mongoose.model('Room', roomSchema);