const express = require('express'),
    app = express(),
    http = require('http'),
    server = http.createServer(app),
    session = require('express-session');

//about session
const sessionInit = session({
    secret: '$hash#enCod',
    key: 'entry',
    resave: true,
    saveUninitialized: true,
    cookie: {}
});

//about socket.io
const socket = require('socket.io'),
    sharedSession = require('express-socket.io-session'),
    io = socket(server);

io.use(sharedSession(sessionInit, {autoSave: true}));

//about siofu
const siofu = require('socketio-file-upload');

//socket.io file upload
app.use(siofu.router);

module.exports.app = app;
module.exports.server = server;
module.exports.express = express;
module.exports.sessionInit = sessionInit;
module.exports.io = io;
module.exports.siofu = siofu;