const express = require('express'),
    router = express.Router(),
    control = require('../controllers/test_controller');

router.get('/', control.index);
router.get('/removeUserNotif/:userId', control.fixUserNotif);
router.get('/fixPost/:postId', control.fixPost);

module.exports = router;