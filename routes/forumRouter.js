const express = require('express'),
    control = require('../controllers/forumController'),
    router = express.Router();

router.get('/', control.index);

router.get('/questions', control.questions);
router.get('/question/:questionId', control.question);

router.get('/ask', control.goToAsk);

module.exports = router;