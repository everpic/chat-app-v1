var express = require('express');
var router = express.Router();
var message = require('../controllers/messages_controller');

router.get('/', message.index);

router.get('/getConversations', message.getConversations);

//start a new private conversation
router.get('/getUsers', message.getUsers);
router.post('/privateMessage', message.privateMessage);

//start a new group conversation
router.post('/groupMessage', message.groupMessage);

router.get('/contact/:userId', message.contact);

//select a conversation
router.get('/selectRoom/:roomId', message.selectRoom);

//safe delete of all rooms
router.get('/messages/deleteRooms', message.deleteRooms);

module.exports = router;