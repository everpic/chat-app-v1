const express = require('express'),
    router = express.Router(),
    control = require('../controllers/notificationController');

router.get('/getNotifications', control.getNotifications);
router.post('/subscribe', control.push);

module.exports = router;