const express = require('express'),
    router = express.Router(),
    admin = require('../controllers/adminController'),
    path = require('path');

router.get('/', admin.index);

//Register a new admin (should be a teacher)
router.post('/register', admin.register);

//manage users
router.get('/users/getAll', admin.getUsers);

//manage posts
router.get('/posts/getAll', admin.getPosts);

module.exports = router;