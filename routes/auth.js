const express = require('express'),
    router = express.Router(),
    control = require('../controllers/auth');

router.get('/auth', control.auth);