var express = require('express');
var router = express.Router();
var profile = require('../controllers/profile_controller');

router.get('/:userId', profile.index);

router.post('/update/about', profile.updateAbout);

router.post('/update/scolar', profile.updateScolar);

router.post('/update/addMaterial', profile.addMaterial);
router.get('/update/deleteMaterial/:materialId', profile.deleteMaterial);

router.post('/update/profile', profile.updateProfile);

module.exports = router;