var express = require('express');
var router = express.Router();
var postsController = require('../controllers/posts_controller');

router.get('/', postsController.index);
router.get('/loadTimeLine', postsController.timeLine);

router.get('/like/:postId', postsController.like);

router.post('/comment/:postId', postsController.comment);

router.get('/searchDestination', postsController.searchDestination);
router.post('/share', postsController.share);

router.get('/getOriginalPost/:postId', postsController.giveOriginalPost);

router.post('/postOneFile', postsController.postOneFile);
router.post('/postMultipleFiles', postsController.postMultipleFiles);

router.get('/delete/:postId', postsController.delete);

router.post('/signal', postsController.signal);
router.get('/authorize/:postId', postsController.authorize);

module.exports = router;