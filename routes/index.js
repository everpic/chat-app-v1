var express = require('express');
var router = express.Router();
var mainController = require('../controllers/main_controller');
const hash = require('password-hash-and-salt');
const moment = require('moment');

/* Home page. */
router.get('/', mainController.auth);
router.post('/', mainController.login);

// Subscribe page
router.get('/subscribe', mainController.subscribe);
router.post('/subscribe', mainController.register);

//test template
router.get('/logout', mainController.logout);

//token verification
router.get('/verify/:token', mainController.verify);

module.exports = router;
